// Use Parse.Cloud.define to define as many cloud functions as you want.
// For example:
Parse.Cloud.define("EventWithUsers", function(request, response) { 
  var eventId = request.params.eventId;
  console.log(request.params);
    
  // The object we will return for response.
  var toReturn = new Object();
  // Map userId -> userstatus
  var usersStatus = {};
  // Map userId -> UserEvent <Object Id>
  var userObjectId = {};
  // User Ids
  var participantsUserId = new Array();
  var participants = new Array();
  var eventOwner;
    
  var Events = Parse.Object.extend("Events");
  var UserEvents = Parse.Object.extend("UserEvents");
  var innerQuery = new Parse.Query(Events);
  innerQuery.equalTo("eventId", eventId);
    
  var query = new Parse.Query(UserEvents);
  query.include("event");
  query.matchesQuery("event", innerQuery);
  query.find({
    success: function(userEvents) {
      if (userEvents.length <= 0) {
        response.error("No post found");
      } else {
        var event = userEvents[0].get("event");
        toReturn["event"] = event;
        for (var i = 0; i < userEvents.length; i++) {
          var userId = userEvents[i].get("userId");
  
          if (userId == event.get("eventOwner")) {
            eventOwner = userId;
          }
          var userStatus = userEvents[i].get("userStatus");
          var objectId = userEvents[i].id;
          participantsUserId.push(userId);
          usersStatus[userId] = userStatus;
          userObjectId[userId] = objectId;
        }
                
        var query = new Parse.Query(Parse.User);
        query.containedIn("fbId", participantsUserId);
        query.find({
          success: function (users) {
            for (var i = 0; i < users.length; i++) {
              var user = users[i].toJSON();
              if (eventOwner == users[i].get("fbId")) {
                user["eventOwner"] = "true";
              } else {
                user["eventOwner"] = "false";
              }
              user["userStatus"] = usersStatus[users[i].get("fbId")];
              user["eventId"] = userObjectId[users[i].get("fbId")];
              participants.push(user);
            }
            toReturn["users"] = participants;
            response.success(toReturn);
          },
            
          error: function(error) {
            response.error("Failed to find query user data");
          }
        });
          
          
      }
    },
      
    error: function(error) {
      response.error("Failed to look up Event ID: " + eventId + " Error:" + error);
    }
  });
});
 
Parse.Cloud.define("PushFbIds", function(request, response) { 
  var eventId = request.params.eventId;
  console.log(request.params);
   
  // The object we will return for response.
  var toReturn = new Array();
   
  var Events = Parse.Object.extend("Events");
  var UserEvents = Parse.Object.extend("UserEvents");
   
  var innerQuery = new Parse.Query(Events);
  innerQuery.equalTo("eventId", eventId);
    
  var query = new Parse.Query(UserEvents);
  query.matchesQuery("event", innerQuery);
  query.find({
    success: function(userEvents) {
      for (var i = 0; i < userEvents.length; i++) {
        var userId = userEvents[i].get("userId");
        toReturn.push(userId);
      }
      response.success(toReturn);
    },
     
    error: function(error) {
      response.error("Failed to find query user data");
    }
  });
});